<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="https://bitbucket.org/lombardpress/lombardpress-schema/raw/master/LombardPressODD.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="https://bitbucket.org/lombardpress/lombardpress-schema/raw/master/LombardPressODD.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xmlns:xi="http://www.w3.org/2001/XInclude">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Librum I, Distinctio 28, Quaestio 2</title>
        <author>Thomas Aquinas</author>
        <editor/>
        <respStmt>
          <name>Jeffrey C. Witt</name>
          <resp>TEI Encoder</resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition n="2015.09-dev-master">
          <title>Librum I, Distinctio 28, Quaestio 2</title>
          <date when="2016-03-15">March 15, 2016</date>
        </edition>
      </editionStmt>
      <publicationStmt>
        <publisher/>
        <pubPlace/>
        <availability status="free">
          <p>Public Domain</p>
        </availability>
        <date when="2016-03-15">March 15, 2016</date>
      </publicationStmt>
      <sourceDesc>
        <p>1856 editum</p>
      </sourceDesc>
    </fileDesc>
    <revisionDesc status="draft">
      <listChange>
        <change when="2015-09-06" who="#JW" status="draft" n="2015.09">
          <note type="validating-schema">lbp-0.0.1</note>
        </change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text xml:lang="la">
    <front>
      <div xml:id="includeList">
        <xi:include
          href="https://bitbucket.org/lombardpress/lombardpress-lists/raw/master/workscited.xml"
          xpointer="worksCited"/>
        <xi:include
          href="https://bitbucket.org/lombardpress/lombardpress-lists/raw/master/Prosopography.xml"
          xpointer="prosopography"/>
      </div>
      <div xml:id="starts-on"> </div>
    </front>
    <body>
      <div xml:id="ta-l1d28q2">
        <head>Librum I, Distinctio 28, Quaestio 2</head>
        <div>
          <head>Prooemium</head>
          <p>Deinde quaeritur de imagine; et circa hoc tria quaeruntur: 1 quid sit imago; 2 utrum
            imago in divinis dicatur essentialiter vel personaliter; 3 si dicatur personaliter,
            utrum conveniat filio tantum.</p>
        </div>
        <div type="articulus">
          <head>Articulus 1</head>

          <head type="questionTitle">Utrum definitio imaginis: imago est species indifferens ejus
            rei ad quam imaginatur, sit competens</head>

          <p>Ad primum sic proceditur, et ponitur definitio Hilarii talis: <quote>imago est ejus rei
              ad quam imaginatur, species indifferens</quote>. Videtur autem quod sit incompetens.
            Imago enim est secundum imitationem in exterioribus. Sed species non est de extrinsecis
            rei; immo dicit quidditatem intrinsecam. Ergo male ponitur in definitione imaginis.</p>
          <p>Praeterea, imago proprie dicitur quod est ad imitationem alterius. Sed species
            indifferens duorum non est ad imitationem alterius, immo est id in quo imitatio
            attenditur. Ergo imago non debet dici species, sed habens speciem.</p>
          <p>Item, eorum quae in infinitum distant, non potest esse indifferentia. Sed creatura est
            imago Dei, a quo tamen in infinitum distat. Ergo indifferens male ponitur in definitione
            imaginis.</p>
          <p>Praeterea, in definitionibus non debet esse circulus. Sed ex hac definitione sequitur
            circulus; definit enim imaginem per rem imaginatam, et imaginatum non potest definiri
            nisi per imaginem. Ergo videtur quod male definiat.</p>
          <p>Item, de ratione imaginis est aequalitas et similitudo, ut patet ex alia ejus
            definitione, quod <quote>imago est rei, ad rem coaequandam, indiscreta et unita
              similitudo</quote>. Cum igitur in praedicta definitione nihil ponatur ad aequalitatem
            et similitudinem pertinens, videtur quod sit diminuta.</p>
          <p>Respondeo dicendum, quod ratio imaginis consistit in imitatione; unde et nomen sumitur.
            Dicitur enim imago quasi imitago. De ratione autem imitationis duo consideranda sunt;
            scilicet illud in quo est imitatio, et illa quae se imitantur. Illud autem respectu
            cujus est imitatio, est aliqua qualitas, vel forma per modum qualitatis significata.
            Unde de ratione imaginis est similitudo. Nec hoc sufficit, sed oportet quod sit aliqua
            adaequatio in illa qualitate vel secundum qualitatem vel secundum proportionem; ut patet
            quod in imagine parva, aequalis est proportio partium ad invicem sicut in re magna cujus
            est imago; et ideo ponitur adaequatio in definitione ejus. Exigitur etiam quod illa
            qualitas sit expressum et proximum signum naturae et speciei ipsius; unde non dicimus
            quod qui imitatur aliquem in albedine, sit imago illius; sed qui imitatur in figura,
            quae est proximum signum et expressum speciei et naturae. Videmus enim diversarum
            specierum in animalibus diversas esse figuras. Ex parte autem imitantium duo sunt
            consideranda; scilicet relatio aequalitatis et similitudinis, quae fundatur in illo uno
            in quo se imitantur; et adhuc ulterius ordo: quia illud quod est posterius ad
            similitudinem alterius factum, dicitur imago; sed illud quod est prius, ad cujus
            similitudinem fit alterum, vocatur exemplar, quamvis abusive unum pro alio ponatur. Et
            ideo Hilarius ad significandum ordinem et relationem se imitantium, dixit: <quote>imago
              est ejus rei ad quam imaginatur</quote>; ad designandum vero id in quo est imitatio,
            dixit: <quote>species indifferens</quote>.</p>
          <p>Ad primum igitur dicendum, quod non in imitatione quorumcumque exteriorum est ratio
            imaginis; sed eorum quae sunt signa quodammodo speciei et naturae; et ideo posuit
            speciem potius quam qualitatem.</p>
          <p>Ad secundum dicendum, quod ista definitio data est per causam: non enim illud quod est
            imago, est ipsa species in qua fit imitatio, proprie loquendo; sed indifferentia speciei
            est causa quare dicatur imago. Vel dicatur, quod utrumque potest dici imago; et illud
            quod imitatur, et id in quo est imitatio, quamvis non ita proprie; et sic definit
            Hilarius.</p>
          <p>Ad tertium dicendum, quod unumquodque quantum attingit ad rationem imaginis, tantum
            attingit ad rationem indifferentiae: secundum enim quod differt, non est imago.
            Invenitur tamen quidam gradus perfectionis imaginis. Dicitur enim quandoque imago
            alterius, in quo invenitur aliquid simile qualitati alterius, quae designat et exprimit
            naturam ipsius; quamvis illa natura in ea non inveniatur; sicut lapis dicitur esse imago
            hominis inquantum habet similem figuram, cui non subsistit natura illa cujus est signum;
            et sic imago Dei est in creatura, sicut imago regis in denario, ut dicit Augustinus; et
            sic est imperfectus modus imaginis. Sed perfectior ratio invenitur quando illi qualitati
            quae designat naturam substantiae, subest natura in specie, sicut est imago hominis
            patris in filio suo: quia habet similitudinem in figura, et in natura quam figura
            significat. Sed perfectissima ratio imaginis est quando eamdem numero formam et naturam
            invenimus in imitante cum eo quem imitatur; et sic est filius perfectissima imago
            patris: quia omnia attributa divina, quae sunt per modum qualitatis significata, simul
            cum ipsa natura sunt in filio, non solum secundum speciem, sed secundum unitatem in
            numero.</p>
          <p>Ad quartum dicendum, quod hoc quod dicit: <quote>ejus rei ad quam imaginatur</quote>,
            est circumlocutio exemplaris. Unde ponitur in virtute unius dictionis, et ponitur
            convenienter in definitione imaginis, sicut prius in definitione posterioris, et non e
            converso. Exemplar enim prius est imagine; unde non est ibi circulus.</p>
          <p>Ad quintum dicendum, quod indifferentia speciei intelligitur et similitudo et
            aequalitas, qualis ad imaginem requiritur; unde illae duae definitiones quasi in idem
            redeunt.</p>
        </div>
        <div type="articulus">
          <head>Articulus 2</head>

          <head type="questionTitle">Utrum imago dicatur essentialiter</head>

          <p>Ad secundum sic proceditur. Videtur quod imago non dicatur essentialiter. Ut enim
            supra, dist. 27, dictum est ab Augustino, nihil est absurdius quam imaginem ad se dici.
            Sed illud quod essentialiter dicitur in divinis, ad se dicitur. Ergo absurdum est ut
            essentialiter dicatur.</p>
          <p>Praeterea, imago de ratione sua, ut dictum est, art. praeced., importat ordinem. Sed in
            divinis non est nisi ordo originis. Cum igitur nihil importans originem, in divinis
            essentialiter dicatur, videtur quod nec imago.</p>
          <p>Item, ex absolutis vel essentialibus non potest probari personarum distinctio: quia in
            essentialia Trinitatis potest deducere naturalis ratio; non autem in personarum
            distinctionem. Sed supra, 2 distinct., qu. 1, art. 4, probata est distinctio personarum
            ex ratione imaginis. Ergo imago non dicitur essentialiter.</p>
          <p>Contra est quod in littera dicitur per Augustinum et Hilarium.</p>
          <p>Respondeo dicendum, quod, sicut dictum est, art. praeced., imago potest dici
            dupliciter: vel id quod imitatur aliquem, vel id in quo est imitatio. Si dicatur imago
            prout proprie accipitur id quod imitatur alterum; sic essentia divina non potest dici
            imago, sed exemplar; cujus imago est creatura: quia imago praesupponit ordinem ad
            aliquod principium: essentia autem divina non habet aliquod principium; sed tamen sic
            aliqua persona potest dici imago alterius, inquantum persona praesupponit sibi secundum
            ordinem naturae aliam personam. Unde sic imago, secundum quod proprie de Deo dicitur,
            semper est personale. Si autem dicatur imago id in quo est imitatio, sic natura divina
            est imago; quia in ipsa est duplex imitatio. Una personae ad personam, secundum quod
            filius in natura divina quam habet a patre, imitatur patrem. Alia creaturae ad
            creatorem, inquantum creatura imitatur creatorem, sed imperfecte, secundum aliquam
            similitudinem bonitatis ipsius. Et quantum ad primam imitationem, quae scilicet est
            personae ad personam, imago in recto significabit essentiam, sed in obliquo faciet
            intellectum personarum; sic enim idem erit imago quod natura divina personarum in ea se
            imitantium; et sic accepit supra Hilarius. Unde probavit ex ratione imaginis et unitatem
            essentiae et distinctionem personarum. Sed quantum ad secundam imitationem, quae est
            creaturae ad creatorem, imago significabit divinam essentiam, et connotabit respectum ad
            creaturam quae imitatur ipsam; et sic accepit ibidem Augustinus; unde ex ratione
            imaginis non probavit nisi unitatem essentiae.</p>
          <p>Et per hoc patet solutio ad objecta.</p>
        </div>
        <div type="articulus">
          <head>Articulus 3</head>

          <head type="questionTitle">Utrum spiritus sanctus possit dici imago</head>

          <p>Ad tertium sic proceditur. Videtur quod spiritus sanctus possit dici imago. Primo per
            Damascenum qui dicit, quod spiritus sanctus est imago filii.</p>
          <p>Praeterea, sicut filius imitatur patrem, per omnia consimilis sibi, ita etiam spiritus
            sanctus. Sed hoc est quod requiritur ad perfectam rationem imaginis, ut dictum est, art.
            1 hujus quaest. Ergo spiritus sanctus est imago patris.</p>
          <p>Si dicis, quod spiritus sanctus non habet hoc quod sit similis patri per omnia, ex
            ratione processionis, sicut filius hoc habet inquantum procedit ut genitus; contra: quia
            processio spiritus sancti non tantum est processio amoris, sed processio amoris divini.
            Sed processio amoris divini, inquantum hujusmodi, habet quod sit in plenitudine ejusdem
            naturae. Ergo videtur quod spiritus sanctus ex processione sua habeat quod sit
            imago.</p>
          <p>Praeterea, dicitur absolute, quod spiritus sanctus est aequalis patri, et similis, et
            connaturalis; quamvis hoc non habeat ex ratione suae processionis absolute. Cum igitur
            similitudo et aequalitas et connaturalitas constituant perfectam rationem imaginis;
            videtur quod spiritus sanctus absolute sit imago.</p>
          <p>Contra est quod Hilarius dicit, quod aeternitas est in patre, et species in imagine, et
            usus in munere. Sicut ergo munus vel donum est proprium spiritus sancti, ita imago
            filii. Hoc idem videtur per Augustinum, supra, distinct. 27.</p>
          <p>Respondeo dicendum, quod imago, secundum quod personalis dicitur, convenit tantum
            filio, et non spiritui sancto. Cujus ratio diversimode assignatur. Quidam enim dicunt,
            quod cum imago ponat imitationem in exterioribus, et notionalia in divinis sint quasi
            exteriora, filius convenienter dicitur imago patris, quia imitatur patrem etiam in
            aliqua notione, scilicet in communi spiratione; non autem spiritus sanctus, qui nullam
            notionem communem cum patre habet. Sed hoc non videtur conveniens propter duo: primo,
            quia notionalia in divinis non se magis habent per modum exteriorum quam essentialia,
            praeter illa quae sunt assequentia substantiam, secundum Damascenum; et ideo imitatio in
            illis adhuc faceret rationem imaginis. Secundo, quia secundum relationem originis non
            attenditur in divinis similitudo aut aequalitas, vel dissimilitudo vel inaequalitas, ut
            ex verbis Augustini habitum est supra, 20 distinct. Ad rationem autem imaginis
            requiritur similitudo et aequalitas, ut dictum est, art. 1 hujus quaest. Et ideo alii
            dicunt, quod impossibile est unius rei esse plures imagines immediate ducentes in illam,
            nisi per materiam divisas; nec etiam e contrario est possibile quod idem sit imago
            plurium; et ideo, cum patris imago sit filius, non potest etiam esse imago spiritus
            sanctus: quia sic plures essent imagines unius. Nec item potest esse quod spiritus
            sanctus sit imago patris et filii: quia sic idem esset imago immediata plurium. Istud
            etiam non videtur conveniens propter duo: primo, quia spiritus sanctus refertur ad
            patrem et filium ut ad unum principium; unde posset esse imago eorum ut sunt unum
            principium ejus, sicut homo est imago totius Trinitatis. Secundo, quia non est major
            ratio quare non possunt esse unius plures imagines quam unius plures similes vel
            aequales; hoc enim convenit in divinis, scilicet quod plures sint similes vel aequales
            unius, non per divisionem materiae, sed per distinctionem relationum. Et ideo dicendum
            cum aliis, quod quamvis diversitas rationis attributorum non sufficiat ad distinctionem
            realem processionum, tamen sufficit ad diversas notiones eorum, ut supra, 13 dist.,
            quaest. 1, art. 3, dictum est: et ideo quamvis spiritus sanctus sua processione accipiat
            naturam; quia tamen sua processio non est per modum naturae, non dicitur nec est
            generatio quia generatio est processio per modum naturae; et per consequens non dicitur
            filius. Ita etiam dico quod filius ex ratione processionis suae habet quod sit imago, et
            inquantum procedit ut filius, quia filius dicitur ex hoc quod habet naturam patris; et
            inquantum procedit ut verbum, quia verbum, ut dictum est, dist. 27, quaest. 2, art. 1,
            est quaedam similitudo in intellectu ipsius rei intellectae. Sed spiritus sanctus non
            habet hoc ex ratione suae processionis, quia procedit ut amor; et ideo sicut non dicitur
            filius, quamvis accipiat sua processione naturam patris; ita nec imago, quamvis habeat
            similitudinem ad patrem.</p>
          <p>Ad primum igitur dicendum, quod Damascenus large accipit imaginem pro quacumque
            similitudine.</p>
          <p>Ad secundum dicendum, quod quamvis spiritus sanctus imitetur patrem, non tamen habet ex
            ratione suae processionis ut imago dicatur; et ideo non dicitur imago sicut non dicitur
            filius.</p>
          <p>Ad tertium dicendum, quod identitas rei in divinis non praejudicat distinctioni
            secundum rationem in veritate attributorum; et ideo quamvis sapientia, inquantum est
            divina, sit essentia, nihilominus manet ibi propria ratio sapientiae praeter rationem
            essentiae, et similiter ratio voluntatis praeter rationem naturae et intellectus; et
            propter hoc etiam remanet distinctio in processionibus quae sunt per modum voluntatis et
            intellectus et naturae, ad minus secundum rationem; et diversitas rationum causat
            diversitatem nominum; unde illa nomina nunquam concurrerent in idem, nisi rationes in
            eadem re fundarentur; et quia processio per modum voluntatis et naturae non eidem
            competit in divinis, ideo nec nomina se consequuntur quae proprias rationes processionum
            demonstrant.</p>
          <p>Ad quartum dicendum, quod quamvis nomen imaginis sit impositum ab aequalitate et
            similitudine, tamen est impositum ad significandum rem cui ex modo suae productionis
            competit similitudo et aequalitas; et ideo non oportet quod dicatur absolute similis et
            aequalis nisi ex modo suae processionis hoc habeat quod etiam imago proprie et absolute
            dicatur.</p>
        </div>
        <div>
          <head>Expositio textus</head>
          <p><quote>Quibusdam videtur quod pater solus debeat dici non genitus</quote>. Sciendum,
            quod utraque opinio secundum aliquid verum dicit. Si enim accipiatur <quote>non
              genitus</quote> prout dicit negationem absolute, tunc non convenit patri tantum, nec
            est idem quod ingenitus, quod importat negationem per modum privationis, prout ingenitus
            proprie accipitur. Si autem dicat negationem in genere principii, sic est idem quod
            ingenitus, et tantum patri convenit; et sic procedit prima opinio. <quote>Sed si
              transponas, ut dicas: aliud est patrem esse, aliud filium esse, variatur
              intelligentia</quote>. Ratio dicti est, quia esse dicitur dupliciter: scilicet prout
            significat veritatem compositionis, et secundum quod significat actum essentiae. Quando
            ergo dicitur, aliud est patrem esse, aliud est filium esse, ita quod ly esse sit
            praedicatum dicti; significatur esse quod est accidens essentiae; unde falsa est; quia
            sicut una est essentia trium, ita et unum esse. Cum autem dicitur: aliud est esse
            patrem, aliud filium esse, ita quod ly patrem praedicetur in dicto inesse; significatur
            veritas compositionis, et secundum hoc pater eo est pater quo verum est ipsum dici
            patrem, scilicet paternitate, et non essentia. Et quia alio Deus dicitur pater, scilicet
            paternitate, et alio filius, scilicet filiatione; ideo conceditur quod aliud est esse
            patrem et aliud esse filium, secundum quod ly aliud dicit alietatem notionis, et non
            alietatem essentiae.</p>
        </div>
      </div>
    </body>
  </text>
</TEI>
